$tmp = "$env:temp\GoDNS.zip"
# Store the download into the temporary file
Invoke-WebRequest -OutFile $tmp https://gitlab.com/strategiczone/dyndns-stratzone/-/archive/master/dyndns-stratzone-master.zip
# extract the temporary file (again, this is untested)
$tmp | Expand-Archive -DestinationPath $env:programdata\
# rename download folder
Move-Item $env:programdata\dyndns-stratzone-master $env:programdata\GoDNS -Force
# remove temporary file
$tmp | Remove-Item
# write a message to screen
Write-Host "`n`nAfter editing of config.json run as Admin on PowerShell fallowing command:" -ForegroundColor yellow
Write-Host "Register-ScheduledTask -Xml (get-content $env:programdata\GoDNS\DynDNS-StrategicZone.xml | out-string) -TaskName DynDNS-StrategicZone -Force" -ForegroundColor green
# open explorer 
sleep 6
explorer $env:programdata\GoDNS