# OPNsense IP Aliases Update Automatisation

## OPNsense Firewall Side
Create an Alias with domain names:
- Type: `URL Table (IPs)`
- Content: `http://remote.server/white.list`

On the server `white.list` contains:
```
alice.service.example.com
bob.service.example.com
carol.service.example.com
```

## End Client Side (employee's computer)
We'll use Open Source [GoDNS](https://github.com/TimothyYe/godns), dynamic DNS (DDNS) client tool to update the `A` record with client's Public IP Address every `X` seconds.

 **On `Alice's Windows PC`**
 
### Downlaod & Extract [GoDNS Client](https://gitlab.com/strategiczone/dyndns-stratzone/-/archive/master/dyndns-stratzone-master.zip) to `$env:programdata\GoDNS`

- List of directory `$env:programdata\GoDNS`:
```powershell
PS C:\ProgramData\GoDNS> ls


    Directory: C:\ProgramData\GoDNS


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----         11/2/2020  10:26 PM            506 config.json
-a----         11/2/2020  10:26 PM            832 deploy_godns.ps1
-a----         11/2/2020  10:26 PM           1693 DynDNS-StrategicZone.xml
-a----         10/1/2020  10:26 PM        7669248 godns.exe
-a----         11/2/2020  10:26 PM           2223 README.md


PS C:\ProgramData\GoDNS>
```

### Configure `config.json` with your DNS Provider

- Look [Config Example](https://github.com/TimothyYe/godns#config-example-for-cloudflare) or [config_sample.json](https://github.com/TimothyYe/godns/blob/master/config_sample.json)

Example for `Alice` (`alice.service.example.com`). Content of `config.json`
```json
{
    "provider": "Cloudflare",
    "login_token": "API Token",
    "domains": [{
        "domain_name": "example.com",
        "sub_domains": ["alice","service"]
      }
    ],
    "resolver": "1.1.1.1",
    "ip_url": "https://myip.biturl.top",
    "interval": 300,
    "socks5_proxy": "",
    "notify": {
        "telegram": {
          "enabled": false,
          "bot_api_key": "",
          "chat_id": "",
          "message_template": "Alice has new IP",
          "use_proxy": false
        }
      }
    }
```

### Test
- Execute `godns.exe` and check the output

### Create a service to start at boot with needed triggers
```powershell
Register-ScheduledTask -Xml (get-content $env:programdata\GoDNS\DynDNS-StrategicZone.xml | out-string) -TaskName DynDNS-StrategicZone -Force
```
- Reboot and Enjoy

## Automatically Deploy
Open `PowerShell` as Admin and execute:
```powershell
iwr -useb https://gitlab.com/strategiczone/dyndns-stratzone/-/raw/master/deploy_godns.ps1 | iex
```